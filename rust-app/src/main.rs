//use std::io::prelude::*;
use std::net::TcpStream;
use std::old_io as io;
use std::num::ToPrimitive;
use std::borrow::Cow;
use std::ascii::AsciiExt;
use std::io::Read;
use std::io::Write;
use std::io::BufReader;
use std::io::BufRead;
use std::string::String;

trait BufReaderMtik {
  fn reader(x: Self) -> Self;
  fn read_len(&self) -> u32;
}

impl BufReaderMtik for BufReader<TcpStream> {
  fn reader(x: BufReader<TcpStream>) -> BufReader<TcpStream> { x }
  fn read_len(&self) -> u32 {
    10
  }
}

fn encode_word(s: &str) -> Cow<'static, str> {
  let mut l = s.len();

  if l < 0x80 {
    return Cow::Owned(std::char::from_u32(l.to_u32().unwrap()).unwrap().to_string() + &s);
  } else if l < 0x4000 {
    l |= 0x8000
  } else if l < 0x200000 {
    l |= 0xC00000
  } else if l < 0x10000000 {
    l |= 0xE0000000
  } else {
  }
  
  return Cow::Owned(s.to_string());
}

fn main() {
  let mut stream = TcpStream::connect("10.1.1.1:8728").unwrap();
  stream.write(encode_word("/login").to_string().into_bytes().as_slice());
  stream.write(&[0]);

  let mut buffer = BufReader::new(stream);
  let mut res: Vec<u8> = vec![];
  let mut len = [0; 1];
  buffer.read(&mut len);
  println!("{:?}", vec!(len));

  //println!("read: {}", String::from_utf8(res).ok().unwrap());


  /*let mut data = [0; 256];*/

  /*stream.read(&mut data);

  let mut res = "".to_string();

  std::io::Read::read_to_string(&mut data.as_slice(), &mut res);

  println!("q: {}", [10,13].is_ascii());
  println!("wat: {}", data.as_slice().is_ascii());

  println!("read: {}", res);*/
}