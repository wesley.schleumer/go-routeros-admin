# Index Controller
module.exports = ($scope, $http, $interval) ->
  $scope.leases = null
  $scope.mangle = null
  $scope.CurrentMangle = null
  $scope.CurrentLease = null


  $scope.load = ->
    $http.get('/api').success((res) ->
      $scope.leases = res.Lease
      $scope.mangle = res.Mangle
      $scope.currentMangle = res.CurrentMangle
      $scope.currentLease = res.CurrentLease
    )

  $interval((-> $scope.load()), 5000)
  $scope.load()

  $scope.getMangleStatus = (mangle) ->
    switch mangle.disabled
    | "false"   => "habilitada"
    | "true"    => "desabilitada"

  $scope.disableMangle = (mangle) ->
    mangle.disabled = "true"
    $scope.working = true
    $http.post("/api/set-mangle", { id: mangle['.id'], disabled: "true" }).success ->
      $scope.load()
      $scope.working = false

  $scope.enableMangle = (mangle) ->
    mangle.disabled = "false"
    $scope.working = true
    $http.post("/api/set-mangle", { id: mangle['.id'], disabled: "false" }).success ->
      $scope.load()
      $scope.working = false

  $scope.filterEnabled = (mangle) ->
    return switch mangle.disabled
            | "false"   => true
            | "true"    => false

    #.forEach (item) ->
    #  console.log(item)
    #  item