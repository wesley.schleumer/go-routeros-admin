

module.exports = (routePrefix) ->
  ['$routeProvider', '$locationProvider',
   ($routeProvider, $locationProvider) ->
    $locationProvider.hashPrefix(routePrefix)
  
    $routeProvider
      .when '/',
        templateUrl: '/templates/index.html'
        controller: 'IndexCtrl'
      .otherwise redirectTo: '/error/404'
  
  ]