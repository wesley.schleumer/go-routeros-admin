module.exports = ['$rootScope', '$http', '$log', ($rootScope, $http, $log) ->
  @buildUrl = (endpoint) ->
    "/api/#{endpoint}" |> (-> $log.info("touched #{it}"); it)

  @get = (endpoint, querystring) ->
    url = @buildUrl(endpoint)
    $log.info("started #{url}")
    task = $http({
      url: url,
      method: 'GET',
      params: querystring
    })
    task.then(
      -> $log.info("request to #{url} success"),
      -> $log.error("request to #{url} failed"))
    task

  @post = (endpoint, data) ->
    url = @buildUrl(endpoint)
    $log.info("started #{url}")
    task = $http({
      url: url,
      method: "POST",
      data: data
    })
    task.then(
      -> $log.info("request to #{url} success"),
      -> $log.error("request to #{url} failed"))
    task

  return
]