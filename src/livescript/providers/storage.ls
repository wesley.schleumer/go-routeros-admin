module.exports = [
"$q", ($q) ->
  
  @get = (key) ->
    $q((resolve, reject) -> resolve(localStorage[key] || null))
  
  @set = (key, value) ->
    $q((resolve, reject) -> resolve(localStorage[key] = value))

  @purge = ->
    $q((resolve, reject) -> resolve(localStorage.clear!))    

  return
]