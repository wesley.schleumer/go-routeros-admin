
module.exports = ->
  restrict: 'E',
  transclude: true,
  templateUrl: '/templates/directives/scroller.html'
  link: (scope, element, attrs, controllers, transclude) !->
    ($ element).find('.nano').nanoScroller!