_ = require 'prelude-ls'

module.exports = ['$data', '$moment', '$timeout', ($data, $moment, $timeout) ->
  {
    templateUrl: '/templates/directives/events.html'
    link: (scope, elem, attrs) ->
      scope.events = []

      scope.getEventClasses = (i) -> 
        (switch i.category
          case "event-1" 
            ["event-w2"]
          case "event-2" 
            ["event-w1"]
          default
            ["event-w1"])
        .concat (if i.image then ['with-image'] else [])
        
      scope.getEventStyle = (event) ->
        obj = {}
        if event.image
          obj["background-image"] = "url(#{event.image})"
        obj

      scope.getEventDate = (event) -> $moment.getFromTimestamp(event.data.date)

      events-container = $(elem).find('.events')

      isoOptions = {
        animationEngine: 'css',
        layoutMode: 'masonry',
        itemSelector: '.event',
        masonry: {
          columnWidth: '.grid-sizer',
          itemSelector: '.event',
          gutter: '.gutter-sizer'
        }/*,
        getSortData: {
          weight: (itemElem) ->
            return parseInt(itemElem.getAttribute('priority'));
        },
        sortBy: 'weight'*/
      }

      scope.$watch 'events', ->
        return if scope.events.length < 1
        $timeout (->
          events-container.isotope(isoOptions)
          return
        )
        return

      $data.on 'blocks', (v) ->
        return if not v
        scope.events = v |> _.filter (-> (/^event-/.test it.category))
        return
      return
  }
]