
module.exports = ['$data', '$location', '$rootScope', '$navigate',
($data, $location, $rootScope, $navigate) ->
  {
    templateUrl: '/templates/directives/topbar.html'
    link: (scope, elem, attrs) !->
      scope.links = []
      $data.on 'topbar', (value) !->
        scope.links = value.links if value and value.links


      scope.navigate = (link) !-> $navigate.go(link.url, link.internal)
      scope.getUrl = (link) -> $navigate.build(link.url, link.internal)
        

      filters = $(elem).find(".item > a")

      filters.bind 'click', (e) !->
        #e.stopPropagation!

        filter = $(this)
        
        return if filter.is(".focused")
        
        filter.addClass('focused')
        tooltip = filter.next()
        tooltip.show()
        inner-items = tooltip.find('.close-tooltip, .filter-tooltip-content > ul > li > a')
        current-item = filter.parents('.item')

        hide = ->
          $('body').removeClass('topbar-is-open')

          inner-items.unbind 'tap'
          inner-items.unbind 'focus'
          inner-items.unbind 'focusout'
          current-item.unbind 'mouseleave'

          filter.blur()
          tooltip.hide()
          filter.removeClass('focused')

        current-item.siblings('.item').trigger('topbar:hide')
        
        current-item.unbind('topbar:hide').bind 'topbar:hide' ->
          hide!

        $('body').addClass('topbar-is-open')

        inner-items.one 'click' ->
          hide!

        inner-items.unbind('focus').bind 'focus' ->
          $(this).addClass('focused-item')
        
        current-item.unbind('focusout').bind 'focusout', (e) ->
          if e.relatedTarget
            # damn you, jQuery
            if not $.contains(current-item[0], e.relatedTarget)
              hide!
          $(this).removeClass('focused-item')

        current-item.one 'mouseleave', ->
          hide!
  }
]