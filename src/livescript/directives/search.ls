require('typeahead')
handlebars = require('handlebars')

module.exports = ['$templateStorage', '$location', ($templateStorage, $location) ->
  {
    templateUrl: '/templates/directives/search.html',
    link: (scope, elem, attrs) ->
      engine = new Bloodhound({
        remote: {
          url: '/api/typeahead.json?q=%QUERY',
          filter: (response) ->
            response.result.typeahead
        },
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        limit: 10
      })
      engine.initialize()

      compiled-suggestion = handlebars.compile($templateStorage.get("/templates/typeahead.html"))

      $(elem).find('input.search-input').typeahead(null, {
        highlightSuggestion: true,
        name: 'big-o-search',
        source: engine.ttAdapter(),
        # FIX: Sujo.
        templates: {
          header: '<div class="tt-header">Resultados rápidos:<div>',
          suggestion: compiled-suggestion
        }
      })
      .bind('typeahead:cursorchanged', (ev, data) ->
        if data and data.fulltext
          scope.$apply ->
            scope.query = data.fulltext
      )
      .bind('typeahead:selected typeahead:autocompleted ', (ev, data) ->
        if data and data.url
          scope.$apply ->
            scope.query = data.fulltext
            $location.path(data.url)
      );

  }
]