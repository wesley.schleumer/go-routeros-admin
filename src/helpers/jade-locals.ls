jade = require 'jade'
fs = require 'fs'
walk-sync = require './walk-sync'

locals = ->
  @app-name = 'Mercurio'
  @title = 'Mercurio'
  @root = ""
  @screept = "#{this.root}/js/app.js"

  @templateFiles = ((walk-sync './src/jade/templates/', [], './')
    .filter (file) -> /.jade$/.test(file)
    .map (file) ->
      name: file
      id: file.replace('src/jade', '').replace(/\.jade$/, '.html'))
  @jsValue = (s) -> JSON.stringify(s)
  @renderFile = (file) -> 
    jade.compile(fs.readFileSync(file), {})(this)

module.exports = locals!